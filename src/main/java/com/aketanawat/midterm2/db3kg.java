/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm2;

/**
 *
 * @author Acer
 */
public class db3kg extends dumbbell { // ให้ db3kg รับค่าข้อมูลมาจาก dumbbell

    public db3kg(int size, int count) {//สร้างconstructor dumbbell เพื่อรับค่า size และ count
        super(size, count);// รับค่าsize และ count จาก class dumbbell
    }

    public db3kg() { // สร้าง Overload ของ constructor dumbbell เพื่อรับค่า count และกำหนดให้ size เป็น 0
        super(); // รับค่า count จาก class dumbbell
    }
    

    @Override
    public void time() {// Override Method time เพื่อ สร้าง method times  เก็บค่า size และ count
        super.time(); //รับค่า method time จาก class dumbbell
        if (size == 3) {
            if (count >= 20 && count <= 30) {
                System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
                System.out.println("Somake arms are a bit tired.");
            } else if (count > 30 && count <= 50) {
                System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
                System.out.println("Somake arms are very tired.");
            } else if (count > 50) {
                System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
                System.out.println("Somake arms have no feelings.");
            } else {
                System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
                System.out.println("Somake cheating.");
            }
        }else if (size == 0){
            System.out.println("You didn't select a dumbbell size!!!");
            System.out.println("Available to lift only 3kg, 4kg and 5kg only.");
        } 
        else if (size == 4) {
            System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
            System.out.println("Somying : I'm lifting a dumbbell 4 kg!!!");
        } else if (size == 5) {
            System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
            System.out.println("Somchai : I'm lifting a dumbbell 5 kg!!!");
        } else if (size != 3) {
            System.out.println("Somake choose dumbbell " + size + "kg " + " and " + count + " time.");
            System.out.println("Available to lift only 3kg, 4kg and 5kg only.");
        }

    }

    public void setSizeCount(int size, int count) {//เรียกใช้ข้อมูลทั้ง size และ count โดยใช้ setCountSize
        this.size = size;
        this.count = count;
    }

    public void setSizeCount() { //สร้าง Overload ของ setCountSize เพื่อเก็บค่า count แต่ size เป็น0
        this.size = 0;
        this.count = 0;

    }

}
