/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm2;

/**
 *
 * @author Acer
 */
public class dumbbell {

    protected int size; //size คือขนาดของดัมเบล
    protected int count; //count คือจำนวนครั้งที่ยกดัมเบล
    

    dumbbell(int size, int count) { //สร้างconstructor dumbbell เพื่อรับค่า size และ count
        this.size = size;
        this.count = count;
        
    }

    dumbbell( ) { // สร้าง Overload ของ constructor dumbbell เพื่อรับค่า count และกำหนดให้ size เป็น 0
        this.size = 0;
        this.count = 0;
       
    }
    

    public void time() { //สร้าง Method time เพื่อที่จะบอกว่าเริ่มยกดัมเบล
        System.out.println("Start ---------------------------------");
    }

}
