/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm2;

/**
 *
 * @author Acer
 */
public class TestDumbbell {
    public static void main(String[] args) {
        db3kg somake = new db3kg(3,20); //กำหนดให้สมเอกยกดัมเบล 3 กิโลกรัม และจำนวนครั้งที่ยก
        db3kg somake2 = new db3kg(); //Test Overload ว่าถ้าสมเอกไม่ได้ยกดัมเบลใดเลย ใน Constructer dumbbell
        somake.time();
        somake.setSizeCount(3, 35);
        somake.time();
        somake.setSizeCount(3, 55);
        somake.time();
        somake.setSizeCount(3, 19);
        somake.time();
        somake.setSizeCount(4, 15);
        somake.time();
        somake.setSizeCount(5, 15);
        somake.time();
        somake.setSizeCount(2, 5);
        somake.time();
        somake.setSizeCount();// Tes Overload ว่าถ้าสมเอกไม่ได้ยกดัมเบลใดเลย ในsetSizeCount
        somake.time();
        somake2.time();
        System.out.println("                                            ");
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println("                                            ");
        
        db4kg somying = new db4kg(4,15);
        db4kg somying2 = new db4kg();
        somying.time();
        somying.setSizeCount(4, 27);
        somying.time();
        somying.setSizeCount(4, 36);
        somying.time();
        somying.setSizeCount(4, 14);
        somying.time();
        somying.setSizeCount(3, 15);
        somying.time();
        somying.setSizeCount(5, 20);
        somying.time();
        somying.setSizeCount(8, 5);
        somying.time();
        somying.setSizeCount();
        somying.time();
        somying2.time();
        System.out.println("                                            ");
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println("                                            ");
        
        db5kg somchai = new db5kg(5,8);
        db5kg somchai2 = new db5kg();
        somchai.time();
        somchai.setSizeCount(5, 11);
        somchai.time();
        somchai.setSizeCount(5, 15);
        somchai.time();
        somchai.setSizeCount(5, 7);
        somchai.time();
        somchai.setSizeCount(3, 10);
        somchai.time();
        somchai.setSizeCount(4, 15);
        somchai.time();
        somchai.setSizeCount(7, 8);
        somchai.time();
        somchai.setSizeCount();
        somchai.time();
        somchai2.time();
        
    }
}
